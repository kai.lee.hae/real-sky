# Libraries
from math import pi
import numpy as np

import bpy


# Constants
h = 6.62607004*10**-34 # Planck's constant
c = 299792458 # Speed of light
T = 5778 # Sun's Temperature
k = 1.38064852*10**-23 # Boltzmann constant
n = 1.0002926 # IOR Air
r = 695500000 # Radius of Sun
Hr = 8434.5 # Atmospheric scale height
No = 2.547305*10**25 # Number density of molecules
dep = 0.0279 # Depolarization factor


# Conversion from XYZ to XY
def xyz_from_xy(x, y):
	return np.array((x, y, 1-x-y))

# The CIE colour matching function for 380 - 780 nm in 5 nm intervals
class ColourSystem:
	cmf = 	[[0.0014, 0.0000, 0.0065],
			[0.0022, 0.0001, 0.0105],
			[0.0042, 0.0001, 0.0201],
			[0.0076, 0.0002, 0.0362],
			[0.0143, 0.0004, 0.0679],
			[0.0232, 0.0006, 0.1102],
			[0.0435, 0.0012, 0.2074],
			[0.0776, 0.0022, 0.3713],
			[0.1344, 0.0040, 0.6456],
			[0.2148, 0.0073, 1.0391],
			[0.2839, 0.0116, 1.3856],
			[0.3285, 0.0168, 1.6230],
			[0.3483, 0.0230, 1.7471],
			[0.3481, 0.0298, 1.7826],
			[0.3362, 0.0380, 1.7721],
			[0.3187, 0.0480, 1.7441],
			[0.2908, 0.0600, 1.6692],
			[0.2511, 0.0739, 1.5281],
			[0.1954, 0.0910, 1.2876],
			[0.1421, 0.1126, 1.0419],
			[0.0956, 0.1390, 0.8130],
			[0.0580, 0.1693, 0.6162],
			[0.0320, 0.2080, 0.4652],
			[0.0147, 0.2586, 0.3533],
			[0.0049, 0.3230, 0.2720],
			[0.0024, 0.4073, 0.2123],
			[0.0093, 0.5030, 0.1582],
			[0.0291, 0.6082, 0.1117],
			[0.0633, 0.7100, 0.0782],
			[0.1096, 0.7932, 0.0573],
			[0.1655, 0.8620, 0.0422],
			[0.2257, 0.9149, 0.0298],
			[0.2904, 0.9540, 0.0203],
			[0.3597, 0.9803, 0.0134],
			[0.4334, 0.9950, 0.0087],
			[0.5121, 1.0000, 0.0057],
			[0.5945, 0.9950, 0.0039],
			[0.6784, 0.9786, 0.0027],
			[0.7621, 0.9520, 0.0021],
			[0.8425, 0.9154, 0.0018],
			[0.9163, 0.8700, 0.0017],
			[0.9786, 0.8163, 0.0014],
			[1.0263, 0.7570, 0.0011],
			[1.0567, 0.6949, 0.0010],
			[1.0622, 0.6310, 0.0008],
			[1.0456, 0.5668, 0.0006],
			[1.0026, 0.5030, 0.0003],
			[0.9384, 0.4412, 0.0002],
			[0.8544, 0.3810, 0.0002],
			[0.7514, 0.3210, 0.0001],
			[0.6424, 0.2650, 0.0000],
			[0.5419, 0.2170, 0.0000],
			[0.4479, 0.1750, 0.0000],
			[0.3608, 0.1382, 0.0000],
			[0.2835, 0.1070, 0.0000],
			[0.2187, 0.0816, 0.0000],
			[0.1649, 0.0610, 0.0000],
			[0.1212, 0.0446, 0.0000],
			[0.0874, 0.0320, 0.0000],
			[0.0636, 0.0232, 0.0000],
			[0.0468, 0.0170, 0.0000],
			[0.0329, 0.0119, 0.0000],
			[0.0227, 0.0082, 0.0000],
			[0.0158, 0.0057, 0.0000],
			[0.0114, 0.0041, 0.0000],
			[0.0081, 0.0029, 0.0000],
			[0.0058, 0.0021, 0.0000],
			[0.0041, 0.0015, 0.0000],
			[0.0029, 0.0010, 0.0000],
			[0.0020, 0.0007, 0.0000],
			[0.0014, 0.0005, 0.0000],
			[0.0010, 0.0004, 0.0000],
			[0.0007, 0.0002, 0.0000],
			[0.0005, 0.0002, 0.0000],
			[0.0003, 0.0001, 0.0000],
			[0.0002, 0.0001, 0.0000],
			[0.0002, 0.0001, 0.0000],
			[0.0001, 0.0000, 0.0000],
			[0.0001, 0.0000, 0.0000],
			[0.0001, 0.0000, 0.0000],
			[0.0000, 0.0000, 0.0000]]

	def __init__(self, red, green, blue, white):
		self.red, self.green, self.blue = red, green, blue
		self.white = white
		self.M = np.vstack((self.red, self.green, self.blue)).T 
		self.MI = np.linalg.inv(self.M)
		self.wscale = self.MI.dot(self.white)
		self.T = self.MI / self.wscale[:, np.newaxis]

	def xyz_to_rgb(self, xyz, out_fmt=None):
		rgb = self.T.dot(xyz)
		if np.any(rgb < 0):
			w = - np.min(rgb)
			rgb += w
		if not np.all(rgb==0):
			rgb /= np.max(rgb)
		return rgb

	def spec_to_xyz(self, spec):
		XYZ = np.sum(spec[:, np.newaxis] * self.cmf, axis=0)
		den = np.sum(XYZ)
		if den == 0.:
			return XYZ
		return XYZ / den

	def spec_to_rgb(self, spec):
		xyz = self.spec_to_xyz(spec)
		return self.xyz_to_rgb(xyz)


# Data for Color
illuminant_D65 = xyz_from_xy(0.3127, 0.3291)
cs_srgb = ColourSystem(
						red = xyz_from_xy(0.64, 0.33),
						green = xyz_from_xy(0.30, 0.60),
						blue = xyz_from_xy(0.15, 0.06),
						white = illuminant_D65
						)


def spec_col(lam, AM, distance):
	W = lam*10**-9

	# Planck's Law
	B = (2*pi*h*c**2)/(W**5*(np.exp((h*c)/(k*T*W))-1))*10**-9
	Gsc = B*(r**2/distance**2)
	# Rayleigh Scattering
	trs = 24*pi**3*(Hr/No*W**-4)*((n**2-1)/(n**2+2))**2*((6+3*dep)/(6-7*dep))
	Trs = np.exp(-trs*AM)
	# Irradiance
	Gbn = Gsc*Trs

	return Gbn


def register():
	return None

def unregister():
	return None
